
<p></p><h1>VGC Pokemon</h1> <h3>Android</h3></p><h5>2.0.0</h5>

## ¿Qué es?

<p>Esta APP es un companion que trae los datos de batalla más relevantes de todos los pokemon. Fue creada para conocer a tus rivales aún si no conoces las caracteristicas de su equipo.</p>

<p>En la version 2.0 se añadio el stat builder o calculadora de caracteristicas de combate.</p>

<p> Versión en desarrollo</p>

## Imagenes
![charizard](https://fitopanchodev.cl/laravel8/public/images/1.jpg)
![charizard](https://fitopanchodev.cl/laravel8/public/images/2.jpg)
![leafeon](https://fitopanchodev.cl/laravel8/public/images/3.jpg)
![leafeon](https://fitopanchodev.cl/laravel8/public/images/4.jpg)
![zapdos](https://fitopanchodev.cl/laravel8/public/images/5.jpg)
![kecleon](https://fitopanchodev.cl/laravel8/public/images/6.jpg)


## AMBIENTE

- Node v18.16.0
- react-native 0.72.4
- react 18.2.0


