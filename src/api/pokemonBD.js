import axios from 'axios';

const pokemonDB = axios.create({
    baseURL: 'https://fitopanchodev.cl/laravel8/public/api/nodo2/pokemon/info/'
});


export default pokemonDB;