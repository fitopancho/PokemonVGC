import axios from 'axios';

const typeDB = axios.create({
    baseURL: 'https://fitopanchodev.cl/laravel8/public/api/nodo2/pokemon/type/'
});


export default typeDB;