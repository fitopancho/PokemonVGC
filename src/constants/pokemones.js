import axios from 'axios';

const pokemon = axios.create({
    baseURL: 'https://fitopanchodev.cl/laravel8/public/api/nodo2/pokemon/list'
});


export default pokemon;