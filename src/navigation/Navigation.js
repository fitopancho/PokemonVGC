import {React} from 'react'
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../screens/HomeScreen';
import { BuilderScreen } from '../screens/BuilderScreen';
import { TypeScreen } from '../screens/TypeScreen';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function StackNav() {
  return (
    <Stack.Navigator 
        screenOptions = {{
            headerShown: false,
            cardStyle: {
                backgroundColor: 'white'
            }
        }}
    >
        <Stack.Screen name="HomeScreen"  component={HomeScreen} />
        <Stack.Screen name="TypeScreen"  component={TypeScreen} />
      </Stack.Navigator>
  );
}

export const Navigation = () => {
  return (

    <Tab.Navigator  barStyle={{ backgroundColor: "white" }} 
                    activeColor="#0283C6"
                    inactiveColor="#5AC9EB" 
                    shifting = {false}
                    labeled = {true}
                    
                    screenOptions = { ({route}) => ({
                      tabBarIcon: ({color, focused}) => {
                          let iconName = '';
                          switch(route.name){
                              case 'Buscador':
                                  iconName = 'search'
                              break;
                              case 'Calculadora':
                                  iconName = 'calculator'
                              break;
                          }
                          let colorName = '';
                          if(focused){
                            colorName = '#0283C6'
                          }else{
                            colorName = '#5AC9EB'
                          }
                          
                          return <Icon name={iconName} size={30} color={colorName} />
                      }
                  })
                }>
      <Tab.Screen name="Buscador" component={StackNav}/>
      <Tab.Screen name="Calculadora" component={BuilderScreen} />
    </Tab.Navigator>


    
  );
}