import React,{ useState, useEffect } from 'react'
import {  StyleSheet, TouchableOpacity, Image  } from 'react-native'
import { useNavigation } from '@react-navigation/core';

export const Debilidad = ({deb}) => {
  const navigation = useNavigation();
  const [objType, setObjType] = useState('')

  useEffect(() => {
    setObjType(deb)

  }, []);

  return(<TouchableOpacity
        onPress={() => {
          navigation.navigate('TypeScreen', { id: [objType] })
      }}>
        <Image
              source={{
                uri: `https://fitopanchodev.cl/laravel8/public/assets/img/tipo_${deb.nombre}.png`,
                }}
              style={styles.tinyTypeDeb}
          />
      </TouchableOpacity>)
    
    };
  
const styles = StyleSheet.create({
  tinyTypeDeb: {
    elevation: 8,
    margin: 2,
    width: 30,
    height: 30,
  }
})

export default Debilidad;