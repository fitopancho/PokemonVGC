
import { useState, useEffect } from 'react'
import {  StyleSheet, View, Text, FlatList  } from 'react-native'

import Debilidad from './debilidad';


export const Debilidades = ({deb, texto}) => {

  useEffect(() => {
  }, []);

  return(<View style={styles.debilidad}>
          <Text style={{fontWeight:'bold',color: 'black'}}>{texto}</Text>
          <FlatList
              style={styles.flatTipo}
              numColumns={10}
              horizontal ={false}
              data={deb}
              renderItem={({item}) => <Debilidad deb={item} />}
          />
      </View>
      )
    
    };
  
const styles = StyleSheet.create({
  debilidad: {
    marginLeft: 15
  },
  flatTipo: {
    margin: 10,
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
})

export default Debilidades;
