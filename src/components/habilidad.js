
import {  Text, View   } from 'react-native'

const Habilidad = ({hab}) => (
    <View>
        <Text style={{fontWeight:'bold', color: 'black'}}>
            {hab.nombre}
        </Text>
        <Text style={{ textAlign: 'justify', color: 'black'}}>
            {hab.efecto}
        </Text>
    </View>
    );

export default Habilidad;