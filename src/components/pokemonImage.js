
import {  StyleSheet, View, Image } from 'react-native'

const PokemonImage = ({sprite}) => (
  
  <View style={styles.pokemonSprite }>
    <View style={styles.cardContainer}>
      <View style={styles.pokebolaContainer}>
          <Image
              source={require('../assets/pokebola-blanca.png')}
              style={styles.pokebola}
          />
      </View>
    </View>
    <Image
        style={styles.pokemonImage}
        source={{
            uri: sprite,
            }}
    />

  </View>
);

const styles = StyleSheet.create({
  pokemonSprite: {
    flex: 1,
    alignItems: 'center',
  },
  pokebola: {
    width: 100,
    height: 100,
    position: 'absolute',
    right: -22,
    bottom: -22
    
},
  pokebolaContainer: {
    width: 100,
    height: 100,
    position: 'absolute',
    bottom: 0,
    right: 0,
    overflow: 'hidden',
  },
  pokemonImage:{
    width: 100,
    height: 100,
    position: 'absolute',
    
  },
  cardContainer: {
    marginHorizontal: 10,
    backgroundColor: 'rgba(196,196,196,0.6)',
    borderRadius: 25,
    height: 120,
    width: 150,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
  },
})

export default PokemonImage;

