import React, { Component,  } from 'react';
import { StyleSheet, View } from 'react-native';
import { Table, Row, Rows, } from 'react-native-table-component';


export  class TablaStats extends Component {
    constructor(props) {

      const stats = () => {
        
        return [
          [props.data[0].nm_stat, props.data[0].val_base, props.data[0].val_min, props.data[0].val_max],
          [props.data[1].nm_stat, props.data[1].val_base, props.data[1].val_min, props.data[1].val_max],
          [props.data[2].nm_stat, props.data[2].val_base, props.data[2].val_min, props.data[2].val_max],
          [props.data[3].nm_stat, props.data[3].val_base, props.data[3].val_min, props.data[3].val_max],
          [props.data[4].nm_stat, props.data[4].val_base, props.data[4].val_min, props.data[4].val_max],
          [props.data[5].nm_stat, props.data[5].val_base, props.data[5].val_min, props.data[5].val_max],
          ["Total base", props.data[0].val_base+props.data[1].val_base+props.data[2].val_base+props.data[3].val_base+props.data[4].val_base+props.data[5].val_base, "", ""],
        ]
      }

      super(props);
      this.state = {
        tableHead: ['stat', 'base', 'min', 'max'],
        tableData: stats()
      }
    }
  
    render() {
      const state = this.state;
      return (
        <View style={styles.container}>
          <Table borderStyle={{borderWidth: 1, borderColor: '#c8e1ff'}}>
            <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
            <Rows data={state.tableData} textStyle={styles.text}/>
          </Table>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: { flex: 1, marginBottom: 20, marginTop: 5 },
    head: { height: 40, backgroundColor: 'rgba(196,196,196,0.6)' },
    text: { margin: 6, color: 'black' }
  });


export default TablaStats;