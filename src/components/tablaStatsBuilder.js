import React, { useState, useEffect } from 'react';
import { StyleSheet, View,TextInput, Dimensions } from 'react-native';
import { Text } from 'react-native-paper';
import SelectDropdown from 'react-native-select-dropdown'

const {width: windowWidth} = Dimensions.get('window');

function TablaStatsBuilder(props)  {
    const naturalezas = [
      {name:"Fuerte ", sum: "-", res: "-"}, 
      {name:"Osada (+def/-atk)", sum: "DEF", res: "ATK"},
      {name:"Modesta (+spa/-atk)", sum: "SPA", res: "ATK"}, 
      {name:"Serena (+spd/-atk)", sum: "SPD", res: "ATK"},
      {name:"Miedosa (+spe/-atk)", sum: "SPE", res: "ATK"},
      {name:"Huraña (+atk/-def)", sum: "ATK", res: "DEF"}, 
      {name:"Dócil", sum: "-", res: "-"}, 
      {name:"Afable (+spa/-def)", sum: "SPA", res: "DEF"}, 
      {name:"Amable (+spd/-def)", sum: "SPD", res: "DEF"},
      {name:"Activa (+spe/-def)", sum: "SPE", res: "DEF"},
      {name:"Firme (+atk/-spa)", sum: "ATK", res: "SPA"}, 
      {name:"Agitada (+def/-spa)", sum: "DEF", res: "SPA"}, 
      {name:"Tímida", sum: "-", res: "-"}, 
      {name:"Cauta (+spd/-spa)", sum: "SPD", res: "SPA"},
      {name:"Alegre (+spe/-spa)", sum: "SPE", res: "SPA"},
      {name:"Pícara (+atk/-spd)", sum: "ATK", res: "SPD"}, 
      {name:"Floja (+def/-spd)", sum: "DEF", res: "SPD"}, 
      {name:"Alocada (+spa/-spd)", sum: "SPA", res: "SPD"}, 
      {name:"Rara", sum: "-", res: "-"},
      {name:"Ingenua (+spe/-spd)", sum: "SPE", res: "SPD"},
      {name:"Audaz (+atk/-spe)", sum: "ATK", res: "SPE"}, 
      {name:"Plácida (+def/-spe)", sum: "DEF", res: "SPE"}, 
      {name:"Mansa (+spa/-spe)", sum: "SPA", res: "SPE"}, 
      {name:"Grosera (+spd/-spe)", sum: "SPD", res: "SPE"},
      {name:"Seria", sum: "-", res: "-"}
    ]
    const [nivel, setNivel] = useState("100");
    const [evTotal, setEvTotal] = useState("510");

    const [ivHP, setIvHP] = useState("31");
    const [ivATK, setIvATK] = useState("31");
    const [ivDEF, setIvDEF] = useState("31");
    const [ivSPA, setIvSPA] = useState("31");
    const [ivSPD, setIvSPD] = useState("31");
    const [ivSPE, setIvSPE] = useState("31");

    const [evHP, setEvHP] = useState("0");
    const [evATK, setEvATK] = useState("0");
    const [evDEF, setEvDEF] = useState("0");
    const [evSPA, setEvSPA] = useState("0");
    const [evSPD, setEvSPD] = useState("0");
    const [evSPE, setEvSPE] = useState("0");

    const [statHP, setStatHP] = useState("-");
    const [statATK, setStatATK] = useState("-");
    const [statDEF, setStatDEF] = useState("-");
    const [statSPA, setStatSPA] = useState("-");
    const [statSPD, setStatSPD] = useState("-");
    const [statSPE, setStatSPE] = useState("-");

    const [natATK, setNatATK] = useState("1");
    const [natDEF, setNatDEF] = useState("1");
    const [natSPA, setNatSPA] = useState("1");
    const [natSPD, setNatSPD] = useState("");
    const [natSPE, setNatSPE] = useState("1");

    useEffect(() => {
      calcularStats()
      }, [nivel,ivHP,ivATK,ivDEF,ivSPA,ivSPD,ivSPE,evHP,evATK,evDEF,evSPA,evSPD,evSPE,natATK,natDEF,natSPA,natSPD,natSPE]);

    const updateIV = (stat, nmStat) => {
      let st = ''
      stat = parseInt(stat)
      if(Number.isInteger(stat)){
        if(parseInt(stat) > 31){
          st = "31"
        }else if(parseInt(stat) < 0){
            st = ""
        }else
        {
            st = String(stat)
        } 
      }else{
        st = ""
      }
      
      switch(nmStat){
        case 'ivHP':
          setIvHP(st)
        break;
        case 'ivATK':
          setIvATK(st)
        break;
        case 'ivDEF':
          setIvDEF(st)
        break;
        case 'ivSPA':
          setIvSPA(st)
        break;
        case 'ivSPD':
          setIvSPD(st)
        break;
        case 'ivSPE':
          setIvSPE(st)
        break;
    }
    };

    const updateNivel = (stat) => {
      let lvl = ''
      stat = parseInt(stat)
      if(Number.isInteger(stat)){
        if(parseInt(stat) > 100){
          lvl = "100"
        }else if(parseInt(stat) < 1){
          lvl = ""
        }else
        {
          lvl = String(stat)
        } 
      }else{
        lvl = ""
      };
      setNivel(lvl)
    }

    const updateNat = (nat) => {
        setNatATK('1')
        setNatDEF('1')
        setNatSPA('1')
        setNatSPD('1')
        setNatSPE('1')
        switch(nat.sum){    
          case 'ATK':
            setNatATK('1.1')
          break;
          case 'DEF':
            setNatDEF('1.1')
          break;
          case 'SPA':
            setNatSPA('1.1')
          break;
          case 'SPD':
            setNatSPD('1.1')
          break;
          case 'SPE':
            setNatSPE('1.1')
          break;
      }
      switch(nat.res){    
        case 'ATK':
          setNatATK('0.9')
        break;
        case 'DEF':
          setNatDEF('0.9')
        break;
        case 'SPA':
          setNatSPA('0.9')
        break;
        case 'SPD':
          setNatSPD('0.9')
        break;
        case 'SPE':
          setNatSPE('0.9')
        break;
    }
    }

    const updateEV = (stat, nmStat) => {
      stat = parseInt(stat)
      let st = ''
      if(Number.isInteger(stat)){
        if(parseInt(stat) > 252){
          st = "252"
        }else if(parseInt(stat) < 0){
          st = ""
        }else{
          st = String(stat)
        }
      }else{
        st = ""
      }
      switch(nmStat){
        case 'evHP':
          setEvHP(st)
        break;
        case 'evATK':
          setEvATK(st)
        break;
        case 'evDEF':
          setEvDEF(st)
        break;
        case 'evSPA':
          setEvSPA(st)
        break;
        case 'evSPD':
          setEvSPD(st)
        break;
        case 'evSPE':
          setEvSPE(st)
        break;
    }
    };

    const calcularStats = () => {
      setStatHP(calcularPs(nivel, ivHP, evHP, props.data[0].val_base))
      setStatATK(calcularStat(nivel, ivATK, evATK, natATK, props.data[1].val_base))
      setStatDEF(calcularStat(nivel, ivDEF, evDEF, natDEF, props.data[2].val_base))
      setStatSPA(calcularStat(nivel, ivSPA, evSPA, natSPA, props.data[3].val_base))
      setStatSPD(calcularStat(nivel, ivSPD, evSPD, natSPD, props.data[4].val_base))
      setStatSPE(calcularStat(nivel, ivSPE, evSPE, natSPE, props.data[5].val_base))
      setEvTotal(calcularEvTotal())
    }
    const calcularEvTotal = () => {
      let hp = evHP
      let atk = evATK
      let def = evDEF
      let spa = evSPA
      let spd = evSPD
      let spe = evSPE
      if(hp == ''){
        hp = "0"
      }
      if(atk == ''){
        atk = "0"
      }
      if(def == ''){
        def = "0"
      }
      if(spa == ''){
        spa = "0"
      }
      if(spd == ''){
        spd = "0"
      }
      if(spe == ''){
        spe = "0"
      }
      let total = 510
      return total - (parseInt(hp)+parseInt(atk)+parseInt(def)+parseInt(spa)+parseInt(spd)+parseInt(spe))
    }

    const calcularPs = (nivel, iv, ev, base) => {
      if(nivel == ''){
        nivel = 1
      }
      nivel = parseInt(nivel)
      if(iv == ''){
        iv = 1
      }
      iv = parseInt(iv)
      if(ev == ''){
        ev = 0
      }
      ev = parseInt(ev)
      base = parseInt(base)

      let ps = ((((2*base)+iv+(ev/4))*nivel)/100)+nivel+10
      return parseInt(ps)
    }
    const calcularStat = (nivel, iv, ev, nat, base) => {
      if(nivel == ''){
        nivel = 1
      }
      nivel = parseInt(nivel)
      if(iv == ''){
        iv = 1
      }
      iv = parseInt(iv)
      if(ev == ''){
        ev = 0
      }
      ev = parseInt(ev)
      base = parseInt(base)
      let stat = (((((2*base)+iv+(ev/4))*nivel)/100)+5)*nat
      return parseInt(stat)
    }
      return (
        <View style={styles.container}>
          <View style={styles.row}>
            <Text style={styles.text}>Nivel</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateNivel(stat)}
              value={nivel}
              style={styles.input}
            />

          </View>
          <View style={styles.row}>
            <Text style={styles.text}>Naturaleza</Text>
            <SelectDropdown
              buttonStyle={styles.select}
              defaultButtonText="Selecciona"
              data={naturalezas}
              onSelect={(selectedItem, index) => {
                updateNat(selectedItem)
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem.name
              }}
              rowTextForSelection={(item, index) => {
                return item.name
              }}
            />

          </View>
          <View style={styles.row}>
            <Text style={styles.head}>stat</Text>
            <Text style={styles.head}>puntos base</Text>
            <Text style={styles.head}>IVs</Text>
            <Text style={styles.head}>EVs</Text>
            <Text style={styles.head}>total</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>{props.data[0].nm_stat}</Text>
            <Text style={styles.text}>{props.data[0].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivHP')}
              value={ivHP}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evHP')}
              value={evHP}
              style={styles.input}
            />
            <Text style={styles.text}>{statHP}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>Ataque</Text>
            <Text style={styles.text}>{props.data[1].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivATK')}
              value={ivATK}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evATK')}
              value={evATK}
              style={styles.input}
            />
            <Text style={styles.text}>{statATK}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>Defensa</Text>
            <Text style={styles.text}>{props.data[2].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivDEF')}
              value={ivDEF}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evDEF')}
              value={evDEF}
              style={styles.input}
            />
            <Text style={styles.text}>{statDEF}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>Ataque Esp.</Text>
            <Text style={styles.text}>{props.data[3].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivSPA')}
              value={ivSPA}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evSPA')}
              value={evSPA}
              style={styles.input}
            />
            <Text style={styles.text}>{statSPA}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>Defensa Esp.</Text>
            <Text style={styles.text}>{props.data[4].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivSPD')}
              value={ivSPD}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evSPD')}
              value={evSPD}
              style={styles.input}
            />
            <Text style={styles.text}>{statSPD}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textStat}>{props.data[5].nm_stat}</Text>
            <Text style={styles.text}>{props.data[5].val_base}</Text>
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateIV(stat.replace(/[^0-9]/g, ''), 'ivSPE')}
              value={ivSPE}
              style={styles.input}
            />
            <TextInput
              editable
              inputMode="numeric"
              maxLength={40}
              onChangeText={stat => updateEV(stat.replace(/[^0-9]/g, ''), 'evSPE')}
              value={evSPE}
              style={styles.input}
            />
            <Text style={styles.text}>{statSPE}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.last}></Text>
            <Text style={styles.last}></Text>
            <Text style={styles.last}></Text>
            <Text style={evTotal < 0 ? styles.textRed : styles.text}>{evTotal}</Text>
            <Text style={styles.last}></Text>
          </View>
        </View>
      )
    
  }
  
  const styles = StyleSheet.create({
    container: { flex: 1, marginBottom: 20, marginTop: 5 },
    head: { margin: 1,height: 40, backgroundColor: 'rgba(196,196,196,0.6)', width: (windowWidth/5)-6, textAlign: 'center' },
    text: { margin: 1, color: 'black', width: (windowWidth/5)-6,height: 35, textAlign: 'center', backgroundColor: 'rgba(196,196,196,0.6)' },
    textRed: { margin: 1, color: 'red', width: (windowWidth/5)-6,height: 35, textAlign: 'center', backgroundColor: 'rgba(196,196,196,0.6)' },
    textStat: { margin: 1, color: 'black', width: (windowWidth/5)-6,height: 35, textAlign: 'center' },
    input: { backgroundColor: "white",width: (windowWidth/5)-6, height: 35, textAlign: 'center', margin: 1},
    select: { backgroundColor: "white",width: ((windowWidth/5-4)*3), height: 35, textAlign: 'center', margin: 1},
    last:{margin: 1, color: 'black', width: (windowWidth/5)-6,height: 35, textAlign: 'center', },
    row:{flex:1,flexDirection: 'row',}
  });




export default TablaStatsBuilder;