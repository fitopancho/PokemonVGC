import {  StyleSheet, TouchableOpacity, Image   } from 'react-native'
import { useNavigation } from '@react-navigation/core';

export const Tipo = ({tipo}) => {
    const navigation = useNavigation(); 
    return (
      
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('TypeScreen', { id: [tipo] })
      }}>
      <Image
              source={{
                uri: `https://fitopanchodev.cl/laravel8/public/assets/img/tipo_${tipo.nombre}.png`,
                }}
              style={styles.tinyType}
          />
      </TouchableOpacity>
  );
}


  
  const styles = StyleSheet.create({

    tinyType: {
      elevation: 8,
      marginTop: 30,
      margin: 2,
      width: 50,
      height: 50,
    },
    });
  

export default Tipo;