
import React,{ useEffect, useState } from 'react'
import {  StyleSheet,Text, View, TouchableOpacity, ActivityIndicator, FlatList, ScrollView, Dimensions, DeviceEventEmitter   } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import Autocomplete from 'react-native-autocomplete-input';

import pokemonBD from "../api/pokemonBD";
import pokemon from '../constants/pokemones';
import Tipo from '../components/tipo';
import Habilidad from '../components/habilidad';
import Debilidades from '../components/debilidades';
import PokemonImage from '../components/pokemonImage';
import TablaStats from '../components/tablaStats';

const {height: windowHeight} = Dimensions.get('window');

export const HomeScreen = () => {
  
    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };
    const {top} = useSafeAreaInsets();
    const [isLoading, setIsLoading] = useState(false)
    const [cargaPosterior, setCargaPosterior] = useState(false)
    const [dataPokemon, onChangeDataPokemon] = useState('');
    const [gradient, setGradient] = useState(["#0081C5", "#66D3F0"])
    const [pokes, setPokes] = useState([]);
    const [filteredPokes, setFilteredPokes] = useState([]);
    const [selectedValue, setSelectedValue] = useState('');

    
    useEffect(() => {

      cargarPokes();

      DeviceEventEmitter.addListener('event.testEvent', eventData => {
        setSelectedValue(eventData)
        getResultados(eventData)
      });
    return () => {};   }, []);
    
  const findPoke = (query) => {
    if (query) {
      const regex = new RegExp(`${query.trim()}`, 'i');
      setFilteredPokes(
        pokes.filter((poke) => poke.search(regex) >= 0)
      );
    } else {
      setFilteredPokes([]);
    }
  };

    const cargarPokes = async () =>{
    try{
      setIsLoading(true)
      const request = pokemon.get();
      await Promise.all([
        request
      ]).then((values) => {
          setPokes(values[0].data)
        
        });
          setIsLoading(false)
    }catch(error){
      console.log("fallamos")
      console.log(error)
    }
  }
  
    const getResultados = async (nmPokemon) =>{
        setCargaPosterior(true)
      try{
        setIsLoading(true)
        const request = pokemonBD.get(nmPokemon);
        await Promise.all([
          request
        ]).then((values) => {
            onChangeDataPokemon(values[0].data)
            actualizarColorPantalla(values[0].data)
          });
            setIsLoading(false)
      }catch(error){
        console.log("fallamos")
        console.log(error)
      }
    }
    
    const actualizarColorPantalla = (data) =>{
      try{
        gradColor = []
        data.tipo.forEach(tipo => {
            gradColor.push(tipo.color);
        });
        if(gradColor.length == 1){
            gradColor.push(gradColor[0])
        }
        setGradient(gradColor)     
      }catch(error){
        console.log("fallamos")
        console.log(error)
      }
    }
    return (
    <ScrollView style={{flex: 1}}>
      <LinearGradient
          colors={gradient}
          style={styles.containerGradient}
      >
        <View  style={{marginTop: top}}>
          <View style={styles.container_name}>
            <Text style={{fontWeight: 'bold', fontSize: 30, color: 'black'}}>VGC Companion</Text>
          </View>
          <View style={styles.container_buttons}>
            <Autocomplete
                style={styles.input}
                autoCapitalize="none"
                autoCorrect={false}
                containerStyle={styles.autocompleteContainer}
                data={filteredPokes}
                defaultValue={
                    JSON.stringify(selectedValue) === '{}' ?
                    '' :
                    selectedValue
                }
                onChangeText={(text) => findPoke(text)}
                placeholder="Buscar Pokémon"
                flatListProps={{
                    keyExtractor: (_, idx) => idx,
                    renderItem:({item}) => (
                        <TouchableOpacity
                        onPress={() => {
                            setSelectedValue(item);
                            getResultados(item);
                            setFilteredPokes([]);
                        }}>
                            <Text style={styles.itemText}>
                                {item}
                            </Text>
                        </TouchableOpacity>
                    ),
                  }}
                
                />
          </View>
          <View style={styles.containerData}>
          {cargaPosterior
            ?isLoading
              ?<ActivityIndicator size={35} color="white" style={{marginTop: 20}}/>
              :
                <View style={styles.pokemonContainer}>
                  <View style={styles.row1}>
                    <View style={styles.pokemonType}>
                      <FlatList
                          style={styles.flatTipo}
                          horizontal ={true}
                          data={dataPokemon.tipo}
                          renderItem={({item}) => <Tipo tipo={item} />}
                      />
                    </View>
                    <PokemonImage sprite={dataPokemon.sprite}/>
                  </View>
                  <View style={styles.row2}>   
                    <View style={styles.pokemonHabilidades}>
                      <Text style={{fontWeight:'bold',color: 'black', marginLeft: 15}}>Habilidades</Text>
                      <FlatList
                          style={styles.flatHabilidades}
                          ItemSeparatorComponent={<Text>------</Text> }
                          horizontal ={false}
                          data={dataPokemon.habilidades}
                          renderItem={({item}) => <Habilidad hab={item} />}
                      />
                    </View>
                  </View>
                  <View  style={styles.row3}>
                    <Debilidades deb={dataPokemon.debilidades.cero} texto = "Daño recibido x 0"/>
                    <Debilidades deb={dataPokemon.debilidades.cuarto} texto = "Daño recibido x 1/4"/>
                    <Debilidades deb={dataPokemon.debilidades.medio} texto = "Daño recibido x 1/2"/>
                    <Debilidades deb={dataPokemon.debilidades.normal} texto = "Daño recibido x 1"/>
                    <Debilidades deb={dataPokemon.debilidades.doble} texto = "Daño recibido x 2"/>
                    <Debilidades deb={dataPokemon.debilidades.cuatro} texto = "Daño recibido x 4"/>
                  </View>
                  <View style={styles.row4}>
                    <View>
                      <View>
                        <Text style={{fontWeight:'bold',color: 'black', marginLeft: 20}}>Stats lvl 50, 31 IV, 252 EV, naturaleza favorable</Text>
                      </View>
                      <TablaStats data = {dataPokemon.stats}/>
                    </View>
                    
                    
                  </View>
                </View>
              :''}
          </View>
        </View > 
      </LinearGradient>
    </ScrollView>
    )
}

const styles = StyleSheet.create({
  containerGradient:{
    flex: 1,
    minHeight: windowHeight
  },
  container_name: {
    padding: 10,
    alignItems: 'center',
},
  containerData: {
    flex:1,
    width: '100%',
  },
  container_buttons: {
      padding: 10,
  },
  buttons:{
    alignItems: 'center',
    padding: 0,
    margin: 10,
  },
  appButtonContainer: {
    elevation: 8,
    borderRadius: 25,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  tinyLogo: {
    width: 100,
    height: 100,
  },
  text_Button:{
    color: 'white'
  },
  pokemonContainer: {
    flex: 1,
  },
  row1: {
    flex:1,
    flexDirection: 'row',
  },
  row2: {
    flex:1,
    flexDirection: 'row',
    backgroundColor: 'rgba(196,196,196,0.6)',
    borderRadius: 25,
    marginHorizontal: 10,
  },
  row3: {
    flex:2,
    backgroundColor: 'rgba(196,196,196,0.6)',
    borderRadius: 25,
    marginTop: 5,
    marginHorizontal: 10,
  },
  row4: {
    flex:1,
    backgroundColor: 'rgba(196,196,196,0.6)',
  
    borderRadius: 25,
    marginTop: 5,
    marginHorizontal: 10,
  },
  pokemonType: {
    flex:1,
    alignItems: 'center',
    marginHorizontal: 10,
    backgroundColor: 'rgba(196,196,196,0.6)',
    borderRadius: 25,
    height: 120,
    width: 150,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
  },
  pokemonName: {
    flex: 1,
    alignItems: 'center',
  },

  flatHabilidades: {
    margin: 10
    },
  flatTipo: {
    margin: 10,
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  pokemonHabilidades: {
    flex:1,
    alignItems: 'left',
  },
  itemText: {
    fontSize: 15,
    paddingTop: 15,
    paddingBottom: 5,
    margin: 2,
    color: 'black'
  },
  input: {
    color: 'black'
  },

  });
