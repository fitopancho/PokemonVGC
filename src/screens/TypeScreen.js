
import React,{ useState, useEffect } from 'react'
import {  StyleSheet,Text, View, TouchableOpacity, Dimensions, FlatList, DeviceEventEmitter   } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
const {height: windowHeight} = Dimensions.get('window');
import { useNavigation } from '@react-navigation/core';

import PokemonImage from '../components/pokemonImage'
import typeDB from '../api/typeDB';


export const TypeScreen = (id) => {

    const navigation = useNavigation();
    const [pokemonList, setPokemonList] = useState([])
    const [nmType, setNmType] = useState(id.route.params.id[0].nombre_es)
    const [nmTypeEn, setNmTypeEn] = useState(id.route.params.id[0].nombre)
    const [gradient, setGradient] = useState([id.route.params.id[0].color, id.route.params.id[0].color])
    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };
    


    useEffect(() => {
        
        cargarTipo()
          
      }, [setPokemonList]);

      const cargarTipo = async () =>{
        try{
          
          const request = typeDB.get(nmTypeEn);
          await Promise.all([
            request
          ]).then((values) => {
            setPokemonList(values[0].data.pokemon);
            
            });
            
              
        }catch(error){
          console.log("fallamos")
          console.log(error)
        }
      }

    return (
        
    <View style={{flex: 1}}>
      <LinearGradient
          colors={gradient}
          style={styles.containerGradient}
      >
        <View  style={styles.row3}>
            <View style={styles.pokeCard}>
                <Text style={{fontWeight:'bold',color: 'black', marginLeft: 15, fontSize: 30, textTransform: 'uppercase'}}>Tipo {nmType}</Text>
                <FlatList
                    style={styles.flatTipo}
                    horizontal ={false}
                    numColumns={2}
                    data={pokemonList}
                    renderItem={({item}) => <TouchableOpacity onPress={() => {
                                            DeviceEventEmitter.emit('event.testEvent', item.nombre);
                                            navigation.navigate('HomeScreen')
                                        }}>

                                                <PokemonImage sprite={"https://fitopanchodev.cl/laravel8/public/assets/pokemon/"+item.nombre+".png"} />
                                            </TouchableOpacity>}
            />
            </View>
        </View>
        </LinearGradient>
           
    </View>
    )
}

const styles = StyleSheet.create({
    containerGradient:{
        flex: 1,
        minHeight: windowHeight,
        
    },
    tinyLogo: {
        width: 150,
        height: 150,
    },
    version: {
        width: 150,
        height: 150,
        alignItems: 'center',
    },
    container_name:{
        flex:1,
        padding: 10,
        alignItems: 'center',
    },
    container_name_center:{
        flex:1,
        padding: 10,
        marginTop: windowHeight*0.3
    },

      row3: {
        flex:2,
        backgroundColor: 'rgba(196,196,196,0.6)',
        borderRadius: 25,
        marginTop: 5,
        marginHorizontal: 10,
      },
      pokeCard: {
        marginLeft: 20,
        marginBottom: 40
      },
  });
